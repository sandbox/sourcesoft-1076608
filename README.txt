--- README  -------------------------------------------------------------

SimpleWeather

Written by Pooya Sanooei	http://sanooei.com



--- INSTALLATION --------------------------------------------------------

- Just enable module and visit 'admin/build/block' to drag block to 
prefered region.

- Visit admin/settings/simpleweather to change your city-list drop down
 menu options.


--- Support -------------------------------------------------------------

Found a bug? report it in issue page
<?php
// $Id$
/**
* @file
* Administration page callbacks for the SimpleWeather module.
*/

/**
* SimpleWeather admin page.
*/
function simpleweather_admin_settings($form_state) {
  // Delete section
  $form['simpleweather_delete'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delete a city'),
    '#tree' => TRUE,
  );

  //add None option
  //$citylist = array('' => t('None'));
  //$citylist[' '] =simpleweather_cityname_list();

  $citylist =simpleweather_cityname_list();

  $form['simpleweather_delete']['simpleweather_delete_city'] = array(
    '#type' => 'select',
    '#title' => t('Select the city you want to remove from weather block'),
    '#options' => $citylist,
  );

  $form['simpleweather_delete']['simpleweather_delete_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('delete_city_function'),
  );

// Add city section
  $form['simpleweather_add'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add a city'),
    '#tree' => TRUE,
  );

  $form['simpleweather_add']['simpleweather_add_city'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the city name'),
    '#size' => 15,
    '#maxlength' => 30,
    '#description' => t("Add new city name to weather block"),
    '#required' => FALSE,
  );

  $form['simpleweather_add']['simpleweather_add_xml'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter xml address for city'),
    '#size' => 15,
    '#maxlength' => 30,
    '#description' => t("Add new city xml address to weather block"),
    '#required' => FALSE,
  );

  $form['simpleweather_add']['simpleweather_add_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#submit' => array('add_city_function'),
  );

return $form;
}

function delete_city_function($form, $form_state) {
  // build citylist again
  $citylist =simpleweather_cityname_list();
  //select API returns index number
  $db_cityid=$form_state['values']['simpleweather_delete']['simpleweather_delete_city'];
  //use index number to receive city name
  $db_cityname=$citylist[$db_cityid];
  //delete using city name
  db_query("DELETE FROM {simpleweather} WHERE name='%s'", $db_cityname);

  drupal_set_message("$db_cityname City has beed deleted.");
}

function add_city_function($form, $form_state) {

  $db_cityname=$form_state['values']['simpleweather_add']['simpleweather_add_city'];
  $db_cityxml=$form_state['values']['simpleweather_add']['simpleweather_add_xml'];

  db_query("INSERT INTO {simpleweather} (name, xml) VALUES ('%s','%s')", $db_cityname, $db_cityxml);

  drupal_set_message(t("$db_cityname City has beed added."));
}